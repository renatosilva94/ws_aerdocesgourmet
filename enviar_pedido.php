<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "aerdocesgourmet";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$nome =  htmlspecialchars($_POST["nome"]);
$sobrenome =  htmlspecialchars($_POST["sobrenome"]);
$email =  htmlspecialchars($_POST["email"]);
$descricao =  htmlspecialchars($_POST["descricao"]);
$datahora =  htmlspecialchars($_POST["datahora"]);
$endereco =  htmlspecialchars($_POST["endereco"]);


$sql = "INSERT INTO pedidos (nome, sobrenome, email, descricao, datahora, endereco)
VALUES ('$nome', '$sobrenome', '$email', '$descricao', '$datahora', '$endereco')";

if ($conn->query($sql) === TRUE) {
    $last_id = $conn->insert_id;
    $pedido = array("id" => $last_id,
                        "nome" => $nome,
                        "sobrenome" => $sobrenome,
                        "email" => $email,
                        "descricao" => $descricao,
                        "datahora" => $datahora,
                        "endereco" => $endereco);
    echo json_encode($pedido);

} else {
    $pedido = array("id" => 0,
                        "nome" => "",
                        "sobrenome" => "",
                        "email" => "",
                        "descricao" => "",
                        "datahora" => "",
                        "endereco" => "");
    echo json_encode($pedido);
}

$conn->close();
