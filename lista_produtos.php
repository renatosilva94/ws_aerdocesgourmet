<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "aerdocesgourmet";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT * FROM produtos";
$result = $conn->query($sql);

$doces = array();

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
	$doces[] = array("id" => $row["id"], "nome_produto" => utf8_encode($row["nome_produto"]),
	"preco" => utf8_encode($row["preco"]), "descricao" => utf8_encode($row["descricao"]), "sabores" => utf8_encode($row["sabores"])
);
  }
} 

$conn->close();
header("Content-type: application/json");
echo json_encode($doces, JSON_PRETTY_PRINT);